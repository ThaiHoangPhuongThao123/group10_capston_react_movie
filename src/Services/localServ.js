export let localServ = {
  setUser: (user) => {
    let dataJSON = JSON.stringify(user);
    localStorage.setItem("DATA_LOCAL", dataJSON);
  },
  getUser: () => {
    let dataJSON = localStorage.getItem("DATA_LOCAL");
    return JSON.parse(dataJSON);
  },
  removeUser: () => {
    localStorage.removeItem("DATA_LOCAL");
  },
  setMaChieu: (maLichChieu) => {
    let dataJSON = JSON.stringify(maLichChieu);
    localStorage.setItem("MA_LICH_CHIEU", dataJSON);
  },
  getMaChieu: () => {
    let dataJSON = localStorage.getItem("MA_LICH_CHIEU");
    return JSON.parse(dataJSON);
  },
};
