import axios from "axios";
import { CYBER_TOKEN } from "../utils/config";

export const https = axios.create({
  baseURL: "https://movienew.cybersoft.edu.vn",
  headers: {
    TokenCybersoft: CYBER_TOKEN,
    Authorization: "",
  },
});
