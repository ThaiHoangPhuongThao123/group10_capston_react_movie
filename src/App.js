import "./App.css";
import { Route, Routes } from "react-router-dom";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";
import BookingPage from "./Pages/BookingPage/BookingPage";
import HomePage from "./Pages/HomePage/HomePage";
import HomeLayout from "./Layout/HomeLayout";
import AccountPage from "./Pages/AccountPage/AccountPage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import Page404 from "./Pages/Page404/Page404";

function App() {
  return (
    <Routes>
      <Route path="/" element={<HomeLayout contentPage={<HomePage />} />} />
      <Route path="/register" element={<RegisterPage />} />
      <Route path="/login" element={<LoginPage />} />
      <Route path="/detail/:id" element={<DetailPage />} />
      <Route path="/booking-movie/:id" element={<BookingPage />} />
      <Route path="/account" element={<AccountPage />} />
      <Route path="*" element={<Page404 />} />
    </Routes>
  );
}

export default App;
