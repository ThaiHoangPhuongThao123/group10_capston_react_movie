import React from "react";
import {
  BsFacebook,
  BsGithub,
  BsInstagram,
  BsTelephoneFill,
  BsTwitter,
} from "react-icons/bs";
import { SiGmail } from "react-icons/si";
import { ImLocation2 } from "react-icons/im";

export default function FooterMobile() {
  return (
    <footer className="bg-gray-900 mt-5">
      <div className="container grid grid-cols-1 p-5 space-x-5 space-y-4 text-center">
        <div class="flex flex-col space-y-3">
          <span class="font-bold text-2xl text-red-500">CyberFlix</span>
          <p className=" text-white">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore,
            veniam?
          </p>
          <ul>
            <li className="text-white flex justify-around text-xl">
              <a href="/">
                <BsFacebook />
              </a>
              <a href="/">
                <BsGithub />
              </a>
              <a href="/">
                <BsInstagram />
              </a>
              <a href="/">
                <BsTwitter />
              </a>
            </li>
          </ul>
        </div>
        <div className="space-y-3 text-white">
          <span className="text-2xl font-bold">About us</span>
          <div className="space-y-2">
            <p className="cursor-pointer">Lorem ipsum dolor sit.</p>
            <p className="cursor-pointer">Lorem, ipsum dolor.</p>
            <p className="cursor-pointer">Lorem, ipsum.</p>
            <p className="cursor-pointer">Lorem, ipsum dolor.</p>
          </div>
        </div>
        <div className="space-y-3 text-white">
          <span className="text-2xl font-bold">Our Services</span>
          <div className="space-y-2">
            <p className="cursor-pointer">Website Devs</p>
            <p className="cursor-pointer">Web development</p>
            <p className="cursor-pointer">Google Ads</p>
          </div>
        </div>
        <div className="space-y-3 text-white">
          <span className="text-2xl font-bold">Contact Us</span>
          <div className="space-y-2 flex flex-col items-center">
            <div className="flex items-center space-x-2">
              <SiGmail />{" "}
              <p className="cursor-pointer">contactinfo@gmail.com</p>
            </div>
            <div className="flex items-center space-x-2">
              <BsTelephoneFill /> <p className="cursor-pointer">0123456789</p>
            </div>
            <div className="flex items-center space-x-2">
              <ImLocation2 />{" "}
              <p className="cursor-pointer">
                Cybersoft - Ho Chi Minh City, Vietnam
              </p>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
