import React from "react";
import { useSelector } from "react-redux";
import Lottie from "lottie-react";
import animationSpinner from "./animationSpinner.json";

export default function Spinner() {
  let { isLoading } = useSelector((state) => state.spinnerSlice);

  return isLoading ? (
    <div
      style={{ backgroundColor: "#1C3A4B" }}
      className="h-screen w-screen fixed top-0 left-0 z-20 flex justify-center items-center"
    >
      <Lottie
        style={{ height: 400 }}
        animationData={animationSpinner}
        loop={true}
      />
    </div>
  ) : (
    <></>
  );
}
