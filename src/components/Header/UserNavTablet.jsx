import React from "react";
import { useSelector } from "react-redux";
import { localServ } from "../../Services/localServ";
import { NavLink } from "react-router-dom";

export default function UserNavTablet() {
  let user = useSelector((state) => state.ListOfBoxOfficesSlice.userBooking);
  let btnClass = "px-5 py-2 rounded border-black border bg-white w-30";
  let handleLogOut = () => {
    localServ.removeUser();
    window.location.reload();
  };
  let renderContent = () => {
    if (user) {
      return (
        <>
          <span className="text-white font-bold">
            Hello{" "}
            <NavLink to="/account">
              <strong className="text-red-500 underline">{user.hoTen}</strong>!
            </NavLink>
          </span>
          <button className={btnClass} onClick={handleLogOut}>
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <button
            className={btnClass}
            onClick={() => {
              window.location.href = "/login";
            }}
          >
            Đăng nhập
          </button>
          <button
            onClick={() => {
              window.location.href = "/register";
            }}
            className={btnClass}
          >
            Đăng ký
          </button>
        </>
      );
    }
  };
  return <div className="flex items-center space-x-5">{renderContent()}</div>;
}
