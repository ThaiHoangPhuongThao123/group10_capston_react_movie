import React, { useEffect, useState } from "react";
import UserNav from "./UserNav";
import { Link } from "react-router-dom";
import { FaArrowUp } from "react-icons/fa";

export default function Header() {
  const [scrolling, setScrolling] = useState(false);
  const [showBackToTop, setShowBackToTop] = useState(false);
  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 0) {
        setScrolling(true);
      } else {
        setScrolling(false);
      }

      if (window.scrollY > 300) {
        setShowBackToTop(true);
      } else {
        setShowBackToTop(false);
      }
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const handleBackToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  const header = `fixed top-0 left-0 w-full z-50 ${
    scrolling ? "bg-gray-900" : "bg-black"
  } transition duration-500 ease-in-out`;
  return (
    <div className={header}>
      <div className="container mx-auto h-24 flex justify-between items-center">
        <Link to="/" className="text-4xl font-bold text-red-500">
          CyberFlix
        </Link>
        <UserNav />
      </div>
      <div
        className={`${
          showBackToTop ? "opacity-100" : "opacity-0"
        } transition-opacity duration-300 ease-in-out fixed bottom-10 right-10 p-2 bg-blue-500 rounded-lg cursor-pointer`}
        onClick={handleBackToTop}
      >
        <FaArrowUp className="text-white text-xl" />
      </div>
    </div>
  );
}
