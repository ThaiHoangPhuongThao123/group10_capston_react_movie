import React, { useState } from "react";
import { useSelector } from "react-redux";
import { localServ } from "../../Services/localServ";
import "animate.css";
import { NavLink } from "react-router-dom";

export default function UserNavMobile() {
  const [sidebarVisible, setSidebarVisible] = useState(false);
  let user = useSelector((state) => state.ListOfBoxOfficesSlice.userBooking);
  const btnClass = "px-5 py-2 rounded border-black border bg-white w-30";

  const handleLogOut = () => {
    localServ.removeUser();
    window.location.reload();
  };

  const renderSidebarContent = () => {
    if (user) {
      return (
        <>
          <div className="font-bold">
            Hello{" "}
            <NavLink to="/account">
              <strong className="text-red-500 underline">{user.hoTen}</strong>!
            </NavLink>
          </div>
          <button className={btnClass} onClick={handleLogOut}>
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <button
            className={btnClass}
            onClick={() => {
              window.location.href = "/login";
            }}
          >
            Đăng nhập
          </button>
          <button
            onClick={() => {
              window.location.href = "/register";
            }}
            className={btnClass}
          >
            Đăng ký
          </button>
        </>
      );
    }
  };

  return (
    <div className="flex items-center relative">
      <div
        className="cursor-pointer text-white px-4 text-2xl"
        onClick={() => setSidebarVisible(!sidebarVisible)}
      >
        ☰
      </div>

      {sidebarVisible && (
        <>
          <div
            className="fixed top-0 left-0 w-full h-full bg-black opacity-50"
            onClick={() => setSidebarVisible(false)}
          ></div>

          <div
            className={`animate__animated animate__slideInRight fixed top-0 right-0 h-screen w-60 bg-white p-4 shadow flex flex-col items-center space-y-5`}
          >
            {renderSidebarContent()}
          </div>
        </>
      )}
    </div>
  );
}
