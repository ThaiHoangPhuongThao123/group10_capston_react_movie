import React from "react";
import { Desktop, Mobile, Tablet } from "../../responsive/responsive";
import UserNavDesktop from "./UserNavDesktop";
import UserNavMobile from "./UserNavMobile";
import UserNavTablet from "./UserNavTablet";

export default function UserNav() {
  return (
    <div>
      <Desktop>
        <UserNavDesktop />
      </Desktop>
      <Tablet>
        <UserNavTablet />
      </Tablet>
      <Mobile>
        <UserNavMobile />
      </Mobile>
    </div>
  );
}
