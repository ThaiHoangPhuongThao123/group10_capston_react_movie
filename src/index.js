import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter } from "react-router-dom";
import { configureStore } from "@reduxjs/toolkit";
import ListOfBoxOfficesSlice from "./redux/ListOfBoxOfficesSlice";
import userSlice from "./redux/userSlice";
import movieSlice from "./redux/movieSlice";
import spinnerSlice from "./redux/spinnerSlice";
import { Provider } from "react-redux";

const store = configureStore({
  reducer: {
    ListOfBoxOfficesSlice: ListOfBoxOfficesSlice,
    userSlice: userSlice,
    movieSlice: movieSlice,
    spinnerSlice: spinnerSlice,
  },
});
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
