import { createSlice } from "@reduxjs/toolkit";
import { localServ } from "../Services/localServ";

const initialState = {
  maLichChieu: localServ.getMaChieu(),
};

const movieSlice = createSlice({
  name: "maLichChieu",
  initialState,
  reducers: {
    setMovieSlice: (state, action) => {
      state.maLichChieu = action.payload;
    },
  },
});

export const { setMovieSlice } = movieSlice.actions;

export default movieSlice.reducer;
