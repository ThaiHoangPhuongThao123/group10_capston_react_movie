import { createSlice } from '@reduxjs/toolkit'
import { localServ } from '../Services/localServ';

const initialState = {
    thongTinPhim:{},
    danhSachGhe:[],
    danhSachVe:[],
    userBooking:localServ.getUser(),
}

const ListOfBoxOfficesSlice = createSlice({
  name: "ListOfBoxOfficesSlice",
  initialState,
  reducers: {
    setLogin: (state, action ) => { 
        state.userBooking= action.payload
     } ,
    setSecletioncArr: (state, action ) => { 
        state.danhSachVe = action.payload;
     },
     getDataMovieCinema: (state, action ) => { 
        state.thongTinPhim = action.payload.thongTinPhim;
        state.danhSachGhe = action.payload.danhSachGhe;
      }
  }
});

export const {setSecletioncArr, getDataMovieCinema, setLogin} = ListOfBoxOfficesSlice.actions

export default ListOfBoxOfficesSlice.reducer