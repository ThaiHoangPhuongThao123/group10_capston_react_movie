import React from "react";
import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";

export default function HomeLayout({ contentPage }) {
  console.log("contentPage: ", contentPage);

  return (
    <div class>
      <Header />
      {contentPage}
      <Footer />
    </div>
  );
}
