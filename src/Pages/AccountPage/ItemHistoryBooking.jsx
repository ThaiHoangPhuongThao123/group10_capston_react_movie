import React from "react";
import Moment from "react-moment";
export default function ItemHistoryBooking(props) {
  let { danhSachGhe, ngayDat, tenPhim, thoiLuongPhim, giaVe } = props.item;
  let chainSeat = "Ghế: ";
  return (
    <div className="font-medium">
      <p className="text-red-600">
        Ngày Đặt:
        {
          <Moment format="DD - MM - YYYY | hh: mm" withTitle>
            {ngayDat}
          </Moment>
        }
      </p>
      <h6>Tên Phim: {tenPhim}</h6>
      <p>
        Thời gian: {thoiLuongPhim}, <span>Giá vé: {giaVe}</span>
      </p>
      <p>{danhSachGhe[0].tenHeThongRap}</p>
      {danhSachGhe.forEach((ghe) => {
        let { tenGhe } = ghe;
        chainSeat += tenGhe + " ";
      })}
      <p>{danhSachGhe[0].tenCumRap + "-" + chainSeat}</p>
    </div>
  );
}
