import React, { useEffect, useState } from "react";
import "./AccountPage.css";
import bgAccount from "../../asset/img/bg-account.jpg";
import { localServ } from "../../Services/localServ";
import { https } from "../../Services/config";
import { CYBER_TOKEN } from "../../utils/config";
import { FaRegEyeSlash, FaRegEye } from "react-icons/fa";
import { message } from "antd";
import ItemHistoryBooking from "./ItemHistoryBooking";
import Spinner from "../../components/Spinner/Spinner";
import { batLoading, tatLoading } from "../../redux/spinnerSlice";
import { useDispatch, useSelector } from "react-redux";
import Header from "../../components/Header/Header";
export default function AccountPage() {
  const dispatch = useDispatch();
  const [isShowPassword, setIsShowPassword] = useState(false);
  let { userBooking } = useSelector((state) => {
    return state.ListOfBoxOfficesSlice;
  });
  const [taiKhoanChung, setTaiKhoanChung] = useState({
    ...userBooking,
    thongTinDatVe: [],
  });

  useEffect(() => {
    if (userBooking) {
      dispatch(batLoading(true));
      https
        .post("/api/QuanLyNguoiDung/ThongTinTaiKhoan", userBooking.taiKhoan, {
          headers: {
            Authorization: "Bearer " + userBooking.accessToken,
            TokenCybersoft: CYBER_TOKEN,
          },
        })
        .then((res) => {
          dispatch(tatLoading(false));
          setTaiKhoanChung(res.data.content);
          message.success(res.data.message);
        })
        .catch((err) => {
          dispatch(tatLoading(false));
          console.log("err", err);
        });
    }
  }, []);
  const handleTogglePassword = () => {
    setIsShowPassword(!isShowPassword);
  };
  const handleOnchange = (event) => {
    let { value, name } = event.target;
    setTaiKhoanChung({ ...taiKhoanChung, [name]: value });
  };
  const handleUpdateInformation = (event) => {
    event.preventDefault();
    dispatch(batLoading(true));
    let userUpdate = {
      taiKhoan: taiKhoanChung.taiKhoan,
      matKhau: taiKhoanChung.matKhau,
      email: taiKhoanChung.email,
      soDT: taiKhoanChung.soDT,
      maNhom: taiKhoanChung.maNhom,
      maLoaiNguoiDung: taiKhoanChung.maLoaiNguoiDung,
      hoTen: taiKhoanChung.hoTen,
    };
    https
      .put("/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung", userUpdate, {
        headers: {
          Authorization: "Bearer " + userBooking.accessToken,
          TokenCybersoft: CYBER_TOKEN,
        },
      })
      .then((res) => {
        dispatch(tatLoading(false));
        localServ.setUser({ ...localServ.getUser(), ...userUpdate });
        message.success(res.data.message);
      })
      .catch((err) => {
        dispatch(tatLoading(false));
        console.log("err", err);
      });
  };
  const renderHistoryBooking = () => {
    return thongTinDatVe.map((item) => {
      return (
        <div className="" key={item.maVe}>
          <ItemHistoryBooking item={item} />
        </div>
      );
    });
  };
  let { email, hoTen, loaiNguoiDung, matKhau, soDT, taiKhoan, thongTinDatVe } =
    taiKhoanChung;
  return (
    <div
      className="w-screen max-h-full bg-no-repeat  bg-center bg-cover flex items-center justify-center space-y-16"
      style={{ backgroundImage: `url(${bgAccount})` }}
    >
      <Header />
      <Spinner />
      <div className="w-4/5 h-5/6 py-5 ">
        <div
          className="w-full max-h-2/6  bg-white rounded-md bg-opacity-90 mb-5 "
          style={{
            boxShadow:
              "rgba(0, 0, 0, 0.1) 0px 12px 28px 0px, rgba(0, 0, 0, 0.1) 0px 2px 4px 0px",
          }}
        >
          <div className="acount-item">
            <div className="acount-item-top">
              <h4 className="font-bold text-2xl text-blue-600">
                Cài đặt tài khoản chung
              </h4>
              <p className="text-xl font-medium">Thông tin có thể thay đổi</p>
              <hr className="border border-gray-500" />
            </div>
            <div className="acount-item body">
              <form onSubmit={handleUpdateInformation}>
                <div className="grid md:grid-cols-2 md:gap-6">
                  <div className="relative z-0 w-full mb-6 group">
                    <input
                      type="text"
                      name="taiKhoan"
                      id="taiKhoan"
                      className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                      placeholder=" "
                      required
                      value={taiKhoan}
                    />
                    <label
                      htmlFor="taiKhoan"
                      className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                    >
                      Tài Khoản
                    </label>
                  </div>
                  <div className="relative z-0 w-full mb-6 group">
                    <div className="border border-gray-300 flex items-center">
                      <input
                        type={isShowPassword ? "text" : "password"}
                        name="matKhau"
                        id="matKhau"
                        className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent  appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                        placeholder=" "
                        required
                        value={matKhau}
                        onChange={handleOnchange}
                      />
                      <button type="button" onClick={handleTogglePassword}>
                        {isShowPassword ? <FaRegEye /> : <FaRegEyeSlash />}
                      </button>
                    </div>
                    <label
                      htmlFor="matKhau"
                      className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                    >
                      Mật khẩu
                    </label>
                  </div>
                </div>
                <div className="grid md:grid-cols-2 md:gap-6">
                  <div className="relative z-0 w-full mb-6 group">
                    <input
                      type="text"
                      name="hoTen"
                      id="hoTen"
                      className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                      placeholder=" "
                      required
                      value={hoTen}
                      onChange={handleOnchange}
                    />
                    <label
                      htmlFor="hoTen"
                      className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                    >
                      Họ tên
                    </label>
                  </div>
                  <div className="relative z-0 w-full mb-6 group">
                    <input
                      type="email"
                      name="email"
                      id="email"
                      className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                      placeholder=" "
                      required
                      value={email}
                      onChange={handleOnchange}
                    />
                    <label
                      htmlFor="email"
                      className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                    >
                      Email
                    </label>
                  </div>
                </div>
                <div className="grid md:grid-cols-2 md:gap-6">
                  <div className="relative z-0 w-full mb-6 group">
                    <input
                      type="tel"
                      name="soDT"
                      id="soDT"
                      className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                      placeholder=" "
                      required
                      value={soDT}
                      onChange={handleOnchange}
                    />
                    <label
                      htmlFor="soDT"
                      className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                    >
                      Số điện thoại
                    </label>
                  </div>
                  <div className="relative z-0 w-full mb-6 group">
                    <select
                      name="maLoaiNguoiDung"
                      id="maLoaiNguoiDung"
                      className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                      value={loaiNguoiDung?.tenLoai}
                    >
                      <option value="KhachHang">Khách hàng</option>
                      <option value=" QuanTri"> Quản Trị</option>
                    </select>
                    <label
                      htmlFor="floating_company"
                      className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                    >
                      Mã loại khách hàng
                    </label>
                  </div>
                </div>
                <hr className="border border-gray-500 mb-3" />
                <div className="text-end">
                  <button
                    type="submit"
                    className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Cập nhập
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div
          className="w-full max-h-4/6  bg-white rounded-md bg-opacity-90 py-5 px-3"
          style={{
            boxShadow:
              "rgba(0, 0, 0, 0.1) 0px 12px 28px 0px, rgba(0, 0, 0, 0.1) 0px 2px 4px 0px",
          }}
        >
          <p className="text-xl font-medium">Lịch sử đặt vé</p>
          <hr className="border border-gray-500" />
          <div className="grid md:grid-cols-2 md:gap-6 grid-cols-1 overflow-y-auto">
            {renderHistoryBooking()}
          </div>
        </div>
      </div>
    </div>
  );
}
