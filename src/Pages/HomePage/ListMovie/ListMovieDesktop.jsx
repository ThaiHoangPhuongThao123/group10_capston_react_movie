import React, { useState, useEffect } from "react";
import { https } from "../../../Services/config";
import { Card, Modal, message } from "antd";
import "animate.css/animate.min.css";
import { NavLink } from "react-router-dom";
import "animate.css";
import { FaPlayCircle } from "react-icons/fa";
import { useDispatch } from "react-redux";
import { batLoading, tatLoading } from "../../../redux/spinnerSlice";
import Spinner from "../../../components/Spinner/Spinner";

export default function ListMovieDesktop() {
  let dispatch = useDispatch();
  const { Meta } = Card;
  const [movieArr, setMovieArr] = useState([]);
  const [hoveredCard, setHoveredCard] = useState(null);
  const [selectedTrailer, setSelectedTrailer] = useState(null);
  const getYouTubeVideoID = (url) => {
    const regExp =
      /^(?:(?:https?:)?\/\/)?(?:www\.)?(?:youtube\.com\/(?:watch\?(?:.*&)?v=|v\/|embed\/|e\/)|youtu\.be\/|y\/)([^?&\s]+)/;
    const match = url.match(regExp);
    return match && match[1];
  };

  const handleCardHover = (maPhim) => {
    setHoveredCard(maPhim);
  };

  const handleCardLeave = () => {
    setHoveredCard(null);
  };

  const openModal = (trailerString) => {
    const videoID = getYouTubeVideoID(trailerString);
    setSelectedTrailer(videoID);
  };

  useEffect(() => {
    dispatch(batLoading(true));
    https
      .get("api/QuanLyPhim/LayDanhSachPhim/?maNhom=GP07")
      .then((res) => {
        setMovieArr(res.data.content);
        dispatch(tatLoading(false));
      })
      .catch((err) => {
        console.log(err);
        dispatch(tatLoading(false));
        message.error("Lấy thông tin thất bại!");
      });
  }, []);

  const renderMovieList = () => {
    return movieArr.map((item) => {
      const truncatedDescription =
        item.moTa.split(" ").slice(0, 8).join(" ") +
        (item.moTa.split(" ").length > 8 ? "..." : "");

      return (
        <Card
          key={item.maPhim}
          hoverable
          style={{
            width: "100%",
            maxWidth: "240px",
            height: "auto",
            margin: "0 auto",
          }}
          onMouseEnter={() => handleCardHover(item.maPhim)}
          onMouseLeave={handleCardLeave}
          cover={
            <div className="relative group cursor-pointer">
              <img
                width="100%"
                className="h-80 object-cover"
                alt="example"
                src={item.hinhAnh}
              />
              {hoveredCard === item.maPhim && (
                <>
                  <div className="absolute top-0 left-0 w-full h-full bg-black opacity-40"></div>
                  <div className="absolute top-0 left-0 w-full h-full flex items-center justify-center">
                    <FaPlayCircle className="text-white text-4xl" />
                  </div>
                </>
              )}
            </div>
          }
          onClick={() => {
            openModal(item.trailer);
          }}
        >
          {hoveredCard === item.maPhim ? (
            <>
              <Meta title={item.tenPhim} />
              <NavLink
                className="w-full inline-block text-center rounded bg-red-500 text-white py-3 mt-3  animate__animated animate__bounceIn transition hover:font-bold cursor-pointer"
                to={`/detail/${item.maPhim}`}
              >
                Xem chi tiết
              </NavLink>
            </>
          ) : (
            <Meta
              title={item.tenPhim}
              description={truncatedDescription}
              className="pt-2"
            />
          )}
        </Card>
      );
    });
  };
  return (
    <>
      <Spinner />
      <div className="container mt-5 mx-auto ">
        <p className="font-bold text-2xl p-4">Danh sách phim hay trong tháng</p>
        <div className="grid grid-cols-4 gap-5">
          {renderMovieList()}
          <Modal
            open={selectedTrailer !== null}
            onCancel={() => setSelectedTrailer(null)}
            footer={null}
            destroyOnClose
            width={800}
          >
            <iframe
              title="Trailer"
              width="100%"
              height="400"
              src={`https://www.youtube.com/embed/${selectedTrailer}?autoplay=1`}
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
            />
          </Modal>
        </div>
      </div>
    </>
  );
}
