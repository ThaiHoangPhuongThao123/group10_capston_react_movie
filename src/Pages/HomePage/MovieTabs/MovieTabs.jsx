import React, { useEffect, useState } from "react";
import { https } from "../../../Services/config";
import { Tabs } from "antd";
import moment from "moment/moment";
import { NavLink } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setMovieSlice } from "../../../redux/movieSlice";
import { Desktop, Mobile, Tablet } from "../../../responsive/responsive";
import MovieTabsDesktop from "./MovieTabsDesktop";
import MovieTabsTablet from "./MovieTabsTablet";
import MovieTabsMobile from "./MovieTabsMobile";

export default function MovieTabs() {
  return (
    <div>
      <Desktop>
        <MovieTabsDesktop />
      </Desktop>
      <Tablet>
        <MovieTabsTablet />
      </Tablet>
      <Mobile>
        <MovieTabsMobile />
      </Mobile>
    </div>
  );
}
