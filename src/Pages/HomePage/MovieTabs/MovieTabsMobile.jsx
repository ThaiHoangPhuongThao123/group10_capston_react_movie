import React, { useEffect, useState } from "react";
import { https } from "../../../Services/config";
import { Tabs } from "antd";
import { NavLink } from "react-router-dom";


const onChange = () => {
};
export default function MovieTabsMobile() {
  const [movieSystem, setMovieSysten] = useState([]);
  useEffect(() => {
    https
      .get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP02")
      .then((res) => {
        setMovieSysten(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderDsPhim = (danhSachPhim) => {
    return danhSachPhim.map((phim) => {
      return (
        <div key={phim.maPhim} className="py-3 w-32 h-50">
          <NavLink
            to={`/detail/${phim.maPhim}`}
            className="w-full h-full block"
          >
            <div className="image-container transform transition-transform hover:scale-110 w-full h-full">
              <img
                src={phim.hinhAnh}
                alt="img"
                className="w-full h-full object-cover"
              />
            </div>
          </NavLink>
        </div>
      );
    });
  };
  let renderMovieSystem = () => {
    return movieSystem.map((system, index) => {
      return {
        key: index,
        label: <img className="w-28" src={system.logo} alt="" />,
        children: (
          <Tabs
            tabPosition="top"
            className="max-h-[500px]"
            items={system.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.tenCumRap,
                label: (
                  <div className="text-left w-80 whitespace-normals">
                    <p className="text-green-600 font-bold">
                      {cumRap.tenCumRap}
                    </p>
                    <p className="truncate">{cumRap.diaChi}</p>
                  </div>
                ),
                children: (
                  <div className="max-h-[400px] overflow-y-auto flex justify-center items-center">
                    <div className="grid grid-cols-2 gap-5">
                      {renderDsPhim(cumRap.danhSachPhim)}
                    </div>
                  </div>
                ),
              };
            })}
          ></Tabs>
        ),
      };
    });
  };
  return (
    <div className="container pt-32 mx-auto">
      <Tabs
        tabPosition="top"
        defaultActiveKey="1"
        items={renderMovieSystem()}
        onChange={onChange}
      />
    </div>
  );
}
