import React from "react";
import { Tablet, Desktop, Mobile } from "../../../responsive/responsive";
import "./style.css";
import MovieCarouselDesktop from "./MovieCarouselDesktop";
import MovieCarouselTablet from "./MovieCarouselTablet";
import MovieCarouselMobile from "./MovieCarouselMobile";

export default function MovieCarousel() {
  return (
    <div>
      <Desktop>
        <MovieCarouselDesktop />
      </Desktop>
      <Tablet>
        <MovieCarouselTablet />
      </Tablet>
      <Mobile>
        <MovieCarouselMobile />
      </Mobile>
    </div>
  );
}
