import React from "react";
import Carousel from "react-material-ui-carousel";
import { Paper } from "@mui/material";
import "./style.css";

export default function MovieCarouselDesktop() {
  var items = [
    {
      name: "Dinh Thự Ma Ám",
      image:
        "https://images2.thanhnien.vn/528068263637045248/2023/8/5/dinh-thu-ma-16912330384751342051563.png",
    },
    {
      name: "Insidious",
      image:
        " https://imageio.forbes.com/blogs-images/scottmendelson/files/2018/01/BANNER-4-715x278.jpg?format=jpg&width=960",
    },
    {
      name: "Conan",
      image: "https://touchcinema.com/storage/slider-app/2560wx1440h1.jpg",
    },
  ];
  return (
    <Carousel>
      {items.map((item, i) => (
        <Item key={i} item={item} />
      ))}
    </Carousel>
  );
}

function Item(props) {
  return (
    <Paper>
      <img
        src={props.item.image}
        alt=""
        style={{ position: "relative", width: "100%", height: 500}}
      />
    </Paper>
  );
}
