import React, { useEffect } from "react";
import { https } from "../../Services/config";
import { localServ } from "../../Services/localServ";
import { useDispatch } from "react-redux";
import { setLogin } from "../../redux/ListOfBoxOfficesSlice";
import MovieCarousel from "./MovieCarousel/MovieCarousel";
import Header from "../../components/Header/Header";
import ListMovie from "./ListMovie/ListMovie";
import Spinner from "../../components/Spinner/Spinner";
import MovieTabs from "./MovieTabs/MovieTabs";

export default function HomePage() {
  return (
    <>
      <Spinner />
      <div className="pt-24 min-w-full">
        <MovieCarousel />
        <ListMovie />
        <MovieTabs />
      </div>
    </>
  );
}
