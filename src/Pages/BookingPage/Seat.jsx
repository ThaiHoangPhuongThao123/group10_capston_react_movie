import React, { useState } from "react";
import styleBooking from "./BookingPage.css";
import { useDispatch, useSelector } from "react-redux";
import { setSecletioncArr } from "../../redux/ListOfBoxOfficesSlice";

export default function Seat(props) {
  let { ghe } = props;
  const { danhSachVe } = useSelector((state) => {
    return state.ListOfBoxOfficesSlice;
  });
  let newSelectionArr = [...danhSachVe];

  const [isActive, setIsActive] = useState(true);
  const [selectioSeat, setSelectionSeat] = useState("");
  const dispatch = useDispatch();
  const handleTypeSeat = (ghe) => {
    if (isActive && !ghe.daDat) {
      newSelectionArr.push(ghe);
      setSelectionSeat("gheDaChon");
      setIsActive(!isActive);
      dispatch(setSecletioncArr(newSelectionArr));
    } else {
      let index = newSelectionArr.findIndex((item) => {
        return item.maGhe === ghe.maGhe;
      });

      if (index !== -1) {
        newSelectionArr.splice(index, 1);
        dispatch(setSecletioncArr(newSelectionArr));
      }
      setSelectionSeat("");
      setIsActive(!isActive);
    }
  };
  const renderSeat = () => {
    let { maGhe, tenGhe, loaiGhe, daDat } = ghe;
    let type = loaiGhe.toLowerCase();
    return (
      <button
        key={maGhe}
        className={` ${
          daDat === true ? `gheDaDat ` : `gheNgoi ${type} ${selectioSeat}`
        }`}
        onClick={() => {
          handleTypeSeat(ghe);
        }}
      >
        {daDat ? "X" : `${tenGhe}`}
      </button>
    );
  };

  return <div>{renderSeat()}</div>;
}
