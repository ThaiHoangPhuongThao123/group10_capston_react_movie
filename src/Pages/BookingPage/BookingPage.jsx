import React, { useEffect, useState } from "react";
import "./BookingPage.css";
import { useDispatch, useSelector } from "react-redux";
import Seat from "./Seat";
import bgBooking from "../../asset/img/bg-booking.jpg";
import { https } from "../../Services/config";
import {
  getDataMovieCinema,
  setSecletioncArr,
} from "../../redux/ListOfBoxOfficesSlice";
import { message } from "antd";
import { CYBER_TOKEN } from "../../utils/config";
import { useNavigate } from "react-router-dom";
import { Desktop, Mobile, Tablet } from "../../reponsives/responsiveBooking";
import { batLoading, tatLoading } from "../../redux/spinnerSlice";
import Spinner from "../../components/Spinner/Spinner";
import { localServ } from "../../Services/localServ";
import Header from "../../components/Header/Header";

export default function BookingPage() {
  let maLichChieu = useSelector((state) => state.movieSlice.maLichChieu);
  localServ.setMaChieu(maLichChieu);
  const { danhSachGhe, thongTinPhim, danhSachVe, userBooking } = useSelector(state => state.ListOfBoxOfficesSlice);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [showModal, setShowModal] = useState(false);
  useEffect(() => {
    dispatch(batLoading(true));
    https
      .get(`/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`)
      .then((res) => {
        dispatch(setSecletioncArr([]));
        dispatch(getDataMovieCinema(res.data.content));
        dispatch(tatLoading(false));
      })
      .catch((err) => {
        dispatch(tatLoading(false));
        console.log("err: ", err);
      });
  }, []);
  const renderDanhSachGhe = () => {
    return danhSachGhe?.map((ghe) => {
      return <Seat ghe={ghe} key={ghe.maGhe} />;
    });
  };

  const renderGheDaChon = () => {
    let stringGhe = "";
    let tienGhe = 0;
    danhSachVe.forEach((ghe) => {
      stringGhe += `Ghế ${ghe.tenGhe} ,`;
      tienGhe += ghe.giaVe;
    });
    return { stringGhe, tienGhe };
  };
  const handleBooking = () => {
    if (userBooking && danhSachVe.length !== 0) {
      let { maLichChieu } = thongTinPhim;
      let datVe = {
        maLichChieu,
        danhSachVe,
      };

      https
        .post("/api/QuanLyDatVe/DatVe", datVe, {
          headers: {
            Authorization: "Bearer " + userBooking.accessToken,
            TokenCybersoft: CYBER_TOKEN,
          },
        })
        .then((res) => {
          message.success(
            "Đặt ghế thành công thành công - kiểm tra lịch sử đã đặt"
          );
          setShowModal(true);
        })
        .catch((err) => {
          setShowModal(false);
          console.log("err: ", err);
        });
    } else if (!userBooking) {
      setShowModal(false);
      message.error("Chưa đăng nhập");
      setTimeout(() => {
        navigate("/login");
      }, 1000);
    } else if (danhSachVe.length === 0) {
      setShowModal(false);
      message.error("Chưa chọn ghế");
    }
  };
  let { tenCumRap, tenRap, diaChi, tenPhim, ngayChieu, gioChieu } =
    thongTinPhim || {};
  return (
    <div
      className="w-full lg:h-screen max-h-max grid grid-cols-12 p-10 bg-no-repeat bg-center bg-cover space-y-16"
      style={{ backgroundImage: `url(${bgBooking})` }}
    >
      <Header />
      <Spinner />
      <div className="md:col-span-9 col-span-12">
        <div className="w-4/5 mx-auto rounded-md shadow-2xl text-white">
          <div className="trapezoid"></div>
          <h5 className="text-center">Screen</h5>
          <div className=" flex items-center justify-center py-5">
            <Desktop>
              <div
                className="grid text-base overflow-auto"
                style={{ gridTemplateColumns: "repeat(16, 1fr)" }}
              >
                {renderDanhSachGhe()}
              </div>
            </Desktop>
            <Tablet>
              <div
                className="grid text-xs overflow-auto"
                style={{ gridTemplateColumns: "repeat(12, 1fr)" }}
              >
                {renderDanhSachGhe()}
              </div>
            </Tablet>
            <Mobile>
              <div
                className="grid text-xs overflow-auto"
                style={{ gridTemplateColumns: "repeat(8, 1fr)" }}
              >
                {renderDanhSachGhe()}
              </div>
            </Mobile>
          </div>
          <div className="flex justify-evenly text-white">
            <div className="flex items-center ">
              <div className="gheNgoi"></div>
              <p>Ghế thường</p>
            </div>
            <div className="flex items-center">
              <div className="gheNgoi vip"></div>
              <p>Ghế vip</p>
            </div>
            <div className="flex items-center">
              <div className="gheDaDat"></div>
              <p>Ghế đã đặt</p>
            </div>
          </div>
        </div>
      </div>
      <div className=" md:col-span-3 col-span-12 px-3 space-y-5 shadow-2xl text-white overflow-auto">
        <h3 className="text-center text-green-400 font-bold text-2xl text-right-item">
          {renderGheDaChon().tienGhe} VND
        </h3>
        <hr className="border-gray-400" />
        <div className="flex items-end justify-between font-medium text-right-item">
          <h6>Cụm Rap</h6>
          <p className="text-green-600 w-4/5 text-end ">{tenCumRap}</p>
        </div>
        <hr className="border-gray-400" />
        <div className="flex items-end justify-between font-medium text-right-item">
          <h6>Địa chỉ</h6>
          <p className="text-green-600 w-4/5 text-end ">{diaChi}</p>
        </div>
        <hr className="border-gray-400" />
        <div className="flex items-end justify-between font-medium text-right-item">
          <h6>Rạp</h6>
          <p className="text-green-600 w-4/5 text-end ">{tenRap}</p>
        </div>
        <hr className="border-gray-400" />
        <div className="flex items-end justify-between font-medium text-right-item">
          <h6>Ngày giờ chiêu</h6>
          <p className="text-green-600 w-4/5 text-end">
            {ngayChieu} - <span className="text-red-600">{gioChieu}</span>
          </p>
        </div>
        <hr className="border-gray-400" />
        <div className="flex items-end justify-between font-medium text-right-item">
          <h6>Tên Phim</h6>
          <p className="text-green-600 w-4/5 text-end">{tenPhim}</p>
        </div>
        <hr className="border-gray-400" />
        <div className="flex items-end justify-between font-medium text-right-item">
          <h6>Chon</h6>
          <p className="text-green-600 w-4/5 text-end">
            {renderGheDaChon().stringGhe}
          </p>
        </div>
        <button
          className="w-full py-4 text-white text-right-item bg-blue-800 rounded-md border-2 transition-all duration-700 font-bold text-xl hover:cursor-pointer hover:text-blue-800 hover:bg-white hover:border-blue-800"
          onClick={handleBooking}
        >
          Booking
        </button>

        {/* Popup  */}

        {showModal ? (
          <>
            <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
              <div className="relative w-auto my-6 mx-auto max-w-3xl">
                {/*content*/}
                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                  {/*header*/}
                  <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t  bg-pink-600">
                    <h3 className="text-3xl font-semibold">Vé của bạn</h3>
                  </div>
                  {/*body*/}
                  <div className="relative p-6 flex-auto font-medium">
                    <tr className="text-black  ">
                      <td>Tên Tai Khoản: </td>
                      <td className="text-pink-700">{userBooking.taiKhoan}</td>
                    </tr>
                    <tr className="text-black  ">
                      <td>Rap: </td>
                      <td className="text-pink-700">{tenRap}</td>
                    </tr>
                    <tr className="text-black  ">
                      <td>Thời gian chiếu: </td>
                      <td className="text-pink-700">
                        {ngayChieu} -{" "}
                        <span className="text-green-600">{gioChieu}</span>
                      </td>
                    </tr>
                    <tr className="text-black  ">
                      <td>Số ghế: </td>
                      <td className="text-pink-700">
                        {renderGheDaChon().stringGhe}
                      </td>
                    </tr>
                    <tr className="text-black  ">
                      <td>Tổng tiền: </td>
                      <td className="text-pink-700">
                        {renderGheDaChon().tienGhe}
                      </td>
                    </tr>
                  </div>
                  {/*footer*/}
                  <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                    <button
                      className="bg-pink-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                      type="button"
                      onClick={() => {
                        setTimeout(() => {
                          dispatch(setSecletioncArr([]));
                          navigate("/account");
                        }, 2000);
                        return setShowModal(false);
                      }}
                    >
                      Đồng ý
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
          </>
        ) : null}
      </div>
    </div>
  );
}
