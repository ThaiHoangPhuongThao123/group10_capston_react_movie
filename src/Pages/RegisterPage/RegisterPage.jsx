import React, { useState } from "react";
import Background from "../../asset/img/bg-register.jpg";
import { FaUserLock } from "react-icons/fa";
import { https } from "../../Services/config";
import { GROUPID } from "../../utils/config";
import { message } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
export default function RegisterPage() {
  const [user, setUser] = useState({
    taiKhoan: "",
    matKhau: "",
    email: "",
    soDt: "",
    maNhom: "",
    hoTen: "",
    confirmPassword: "",
  });
  const [err, setErr] = useState({
    taiKhoan: "",
    matKhau: "",
    email: "",
    soDt: "",
    maNhom: GROUPID,
    hoTen: "",
    confirmPassword: "",
  });
  const [valuePass, setValuePass] = useState("");
  let navigate = useNavigate();
  const hanhdleOnchange = (event) => {
    let { id, value, pattern } = event.target;
    setUser({ ...user, [id]: value });
    // kiểm tra rỗng
    if (value.trim() === "") {
      setErr({ ...err, [id]: "This field must not be left blank" });
    } else {
      if (pattern) {
        let regex = new RegExp(pattern);
        let isValidate = regex.test(value);
        if (id === "email") {
          if (!isValidate) {
            setErr({
              ...err,
              [id]: "The email field is not properly formatted",
            });
          } else {
            setErr({ ...err, [id]: "" });
          }
        }
        if (id === "matKhau") {
          if (!isValidate) {
            setErr({
              ...err,
              [id]: "The password field is at least 6 characters including at least 1 numeric character and 1 alphanumeric character",
            });
          } else {
            setValuePass(value);
            setErr({ ...err, [id]: "" });
          }
        }
        if (id === "confirmPassword") {
          if (!isValidate || valuePass !== value) {
            setErr({ ...err, [id]: "Password field mismatch" });
          } else {
            setErr({ ...err, [id]: "" });
          }
        }
        if (id === "hoTen") {
          if (!isValidate) {
            setErr({
              ...err,
              [id]: "The full name field does not contain numbers",
            });
          } else {
            setErr({ ...err, [id]: "" });
          }
        }
        if (id === "soDt") {
          if (!isValidate) {
            setErr({
              ...err,
              [id]: "The phone number field contains only numbers",
            });
          } else {
            setErr({ ...err, [id]: "" });
          }
        }
      } else {
        setErr({ ...err, [id]: "" });
      }
    }
  };
  const handleOnBlur = (event) => {
    let { id, value, pattern } = event.target;
    setUser({ ...user, [id]: value });
    // kiểm tra rỗng
    if (value.trim() === "") {
      setErr({ ...err, [id]: "This field must not be left blank" });
    } else {
      if (pattern) {
        let regex = new RegExp(pattern);
        let isValidate = regex.test(value);
        if (id === "email") {
          if (!isValidate) {
            setErr({
              ...err,
              [id]: "The email field is not properly formatted",
            });
          } else {
            setErr({ ...err, [id]: "" });
          }
        }
        if (id === "matKhau") {
          if (!isValidate) {
            setErr({
              ...err,
              [id]: "The password field is at least 6 characters including at least 1 numeric character and 1 alphanumeric character",
            });
          } else {
            setValuePass(value);
            setErr({ ...err, [id]: "" });
          }
        }
        if (id === "confirmPassword") {
          if (!isValidate || valuePass !== value) {
            setErr({ ...err, [id]: "Password field mismatch" });
          } else {
            setErr({ ...err, [id]: "" });
          }
        }
        if (id === "hoTen") {
          if (!isValidate) {
            setErr({
              ...err,
              [id]: "The full name field does not contain numbers",
            });
          } else {
            setErr({ ...err, [id]: "" });
          }
        }
        if (id === "soDt") {
          if (!isValidate) {
            setErr({
              ...err,
              [id]: "The phone number field contains only numbers",
            });
          } else {
            setErr({ ...err, [id]: "" });
          }
        }
      } else {
        setErr({ ...err, [id]: "" });
      }
    }
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    https
      .post("/api/QuanLyNguoiDung/DangKy", user)
      .then((res) => {
        message.success(res.data.message);
        setTimeout(() => {
          navigate("/");
        }, 2000);
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };
  let { taiKhoan, matKhau, emai, soDt, hoTen, confirmPassword } = user;
  return (
    <div className="w-screen lg:h-screen max-h-max  ">
      <div
        className="w-full h-full bg-center bg-cover"
        style={{ backgroundImage: `url(${Background})` }}
      >
        <div className="md:w-1/2 w-full h-4/5 mx-auto py-28 ">
          <form onSubmit={handleSubmit}>
            <div className="p-6 bg-white space-y-6 rounded-md">
              <div className="flex justify-center">
                <FaUserLock className=" inline-block text-5xl text-blue-600 border-2 rounded-full border-blue-600 text-center " />
              </div>
              <div className="relative z-0 w-full mb-6 group">
                <input
                  type="text"
                  name="taiKhoan"
                  id="taiKhoan"
                  className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent rounded border-2 border-gray-900 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                  placeholder=" "
                  required
                  onChange={hanhdleOnchange}
                  value={taiKhoan}
                />
                <label
                  htmlFor="taiKhoan"
                  className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                >
                  Acount
                </label>
                {err.taiKhoan !== "" ? (
                  <span className="text-red-600 font-bold">{err.taiKhoan}</span>
                ) : (
                  ""
                )}
              </div>
              <div className="relative z-0 w-full mb-6 group ">
                <input
                  type="text"
                  name="email"
                  id="email"
                  className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent rounded border-2 border-gray-900 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                  placeholder=" "
                  onChange={hanhdleOnchange}
                  onBlur={handleOnBlur}
                  value={emai}
                  required
                  pattern="[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$"
                />
                <label
                  htmlFor="email"
                  className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                >
                  Email address
                </label>
                {err.email !== "" ? (
                  <span className="text-red-600 font-bold">{err.email}</span>
                ) : (
                  ""
                )}
              </div>
              <div className="relative z-0 w-full mb-6 group">
                <input
                  type="text"
                  name="matKhau"
                  id="matKhau"
                  className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent rounded border-2 border-gray-900 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                  placeholder=" "
                  required
                  onChange={hanhdleOnchange}
                  onBlur={handleOnBlur}
                  value={matKhau}
                  pattern="(?=.*\d)(?=.*[a-zA-Z]).{6,}$"
                />
                <label
                  htmlFor="matKhau"
                  className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                >
                  Password
                </label>
                {err.matKhau !== "" ? (
                  <span className="text-red-600 font-bold">{err.matKhau}</span>
                ) : (
                  ""
                )}
              </div>
              <div className="relative z-0 w-full mb-6 group">
                <input
                  type="password"
                  name="confirmPassword"
                  id="confirmPassword"
                  className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent rounded border-2 border-gray-900 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                  placeholder=" "
                  required
                  onChange={hanhdleOnchange}
                  onBlur={handleOnBlur}
                  value={confirmPassword}
                  pattern="(?=.*\d)(?=.*[a-zA-Z]).{6,}$"
                />
                <label
                  htmlFor="confirmPassword"
                  className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                >
                  Confirm password
                </label>
                {err.confirmPassword !== "" ? (
                  <span className="text-red-600 font-bold">
                    {err.confirmPassword}
                  </span>
                ) : (
                  ""
                )}
              </div>

              <div className="relative z-0 w-full mb-6 group">
                <input
                  type="text"
                  name="hoTen"
                  id="hoTen"
                  className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent rounded border-2 border-gray-900 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                  placeholder=" "
                  required
                  onChange={hanhdleOnchange}
                  onBlur={handleOnBlur}
                  value={hoTen}
                  pattern="^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễếệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$"
                />
                <label
                  htmlFor="hoTen"
                  className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                >
                  Full name
                </label>
                {err.hoTen !== "" ? (
                  <span className="text-red-600 font-bold">{err.hoTen}</span>
                ) : (
                  ""
                )}
              </div>
              <div className="relative z-0 w-full mb-6 group">
                <input
                  type="text"
                  name="soDt"
                  id="soDt"
                  className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent rounded border-2 border-gray-900 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                  placeholder=" "
                  required
                  onChange={hanhdleOnchange}
                  onBlur={handleOnBlur}
                  value={soDt}
                  pattern="^\d+$"
                />
                <label
                  htmlFor="soDt"
                  className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                >
                  Phone number
                </label>
                {err.soDt !== "" ? (
                  <span className="text-red-600 font-bold">{err.soDt}</span>
                ) : (
                  ""
                )}
              </div>
              <button
                type="submit"
                className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              >
                Register
              </button>
              <NavLink
                to={"/login"}
                className="underline decoration-solid lg:text-xl text-base text-blue-600 block text-end"
              >
                Bạn đã có tài khoản? Đăng nhập
              </NavLink>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
