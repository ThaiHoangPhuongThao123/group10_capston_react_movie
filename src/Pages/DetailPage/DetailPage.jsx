import React from "react";
import { Desktop, Mobile, Tablet } from "../../responsive/responsive";
import DetailPageDesktop from "./DetailPageDesktop";
import DetailPageTablet from "./DetailPageTablet";
import DetailPageMobile from "./DetailPageMobile";

export default function DetailPage() {
  return (
    <div>
      <Desktop>
        <DetailPageDesktop />
      </Desktop>
      <Tablet>
        <DetailPageTablet />
      </Tablet>
      <Mobile>
        <DetailPageMobile />
      </Mobile>
    </div>
  );
}
