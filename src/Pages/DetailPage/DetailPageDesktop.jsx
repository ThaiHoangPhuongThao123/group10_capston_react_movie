import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { Button, Rate, Tabs, Modal, message } from "antd";
import { https } from "../../Services/config";
import Header from "../../components/Header/Header";
import moment from "moment/moment";
import { useDispatch } from "react-redux";
import { setMovieSlice } from "../../redux/movieSlice";
import Lottie from "lottie-react";
import popcorn from "./popcorn.json";
import Footer from "../../components/Footer/Footer";
import { batLoading, tatLoading } from "../../redux/spinnerSlice";
import Spinner from "../../components/Spinner/Spinner";

const onChange = (key) => {
};

const scrollToTabsSection = () => {
  const tabsSection = document.getElementById("lichChieuPhim");
  if (tabsSection) {
    tabsSection.scrollIntoView({ behavior: "smooth" });
  }
};

export default function DetailPageDesktop() {
  let dispatch = useDispatch();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalData, setModalData] = useState(null);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
    setModalData(null);
  };

  const [movieSystem, setMovieSystem] = useState([]);

  useEffect(() => {
    dispatch(batLoading(true));
    https
      .get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`)
      .then((res) => {
        setMovieSystem(res.data.content.heThongRapChieu);
        dispatch(tatLoading(false));
      })
      .catch((err) => {
        console.log(err);
        dispatch(tatLoading(false));
        message.error("Lấy thông tin thất bại!");
      });
  }, []);

  let renderMovieSystem = () => {
    return movieSystem.map((system, index) => {
      return {
        key: index,
        label: (
          <div className="flex items-center">
            <img className="w-10 mx-2" src={system.logo} alt="" />
            <h1 className="font-bold">{system.tenHeThongRap}</h1>
          </div>
        ),
        children: (
          <Tabs
            tabPosition="left"
            items={system.cumRapChieu.map((cumRap) => {
              return {
                key: cumRap.tenCumRap,
                label: (
                  <div className="text-left w-80 whitespace-normals">
                    <p className="text-green-600 font-bold">
                      {cumRap.tenCumRap}
                    </p>
                    <p className="truncate">{cumRap.diaChi}</p>
                  </div>
                ),
                children: (
                  <div className="grid grid-cols-3 w-full gap-5">
                    {cumRap.lichChieuPhim.map((thoiGianChieu, index) => {
                      const dateTime = moment(thoiGianChieu.ngayChieuGioChieu);
                      const formattedDate = dateTime.format("DD-MM-YYYY");
                      const formattedTime = dateTime.format("HH:mm");
                      return (
                        <Button
                          key={index}
                          onClick={() => {
                            showModal();
                            setModalData({
                              ngayChieu: formattedDate,
                              gioChieu: formattedTime,
                              tenCumRap: cumRap.tenCumRap,
                              maLichChieu: thoiGianChieu.maLichChieu,
                              tenPhim: movie.tenPhim,
                              hinhAnh: movie.hinhAnh,
                            });
                            dispatch(setMovieSlice(thoiGianChieu.maLichChieu));
                          }}
                          className="bg-blue-500 text-white rounded h-10 text-center min-w-fit"
                        >
                          {formattedDate} ~ {formattedTime}
                        </Button>
                      );
                    })}
                  </div>
                ),
              };
            })}
          ></Tabs>
        ),
      };
    });
  };
  const [movie, setMovie] = useState({});
  const [trailer, setTrailer] = useState(null);
  let { id } = useParams();
  const getYouTubeVideoID = (url) => {
    const regExp =
      /^(?:(?:https?:)?\/\/)?(?:www\.)?(?:youtube\.com\/(?:watch\?(?:.*&)?v=|v\/|embed\/|e\/)|youtu\.be\/|y\/)([^?&\s]+)/;
    const match = url.match(regExp);
    return match && match[1];
  };
  useEffect(() => {
    https
      .get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`)
      .then((res) => {
        const videoID = getYouTubeVideoID(res.data.content.trailer);
        setTrailer(videoID);
        setMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <>
      <Header />
      <Spinner />
      <div
        className="flex justify-center items-center mt-24"
        style={{
          backgroundImage: `url(
          "https://demo1.cybersoft.edu.vn/static/media/backapp.b46ef3a1.jpg"
        )`,
          backgroundSize: "150%",
          backgroundPosition: "center",
          objectFit: "cover",
        }}
      >
        <div className="container flex p-10">
          <img src={movie.hinhAnh} alt="" width={300} />
          <div className="flex flex-col justify-between p-5">
            <h1 className="text-4xl text-white font-bold">{movie.tenPhim}</h1>
            {Object.keys(movie).length > 0 && (
              <h1 className="text-white font-serif text-2xl">
                Đánh giá:{" "}
                <Rate allowHalf disabled defaultValue={movie.danhGia / 2} /> -
                120 phút
              </h1>
            )}

            <p className="text-white">{movie.moTa}</p>
            <Button
              onClick={() => scrollToTabsSection()}
              className="w-1/3 rounded bg-red-500 text-white p-4 transition hover:scale-105 hover:font-bold hover:bg-gray-100 flex justify-center items-center"
            >
              Đặt vé
            </Button>
          </div>
        </div>
      </div>
      <div
        className="flex justify-center items-center relative py-60"
        style={{
          backgroundImage: `url("https://img.freepik.com/premium-photo/black-brick-wall-loft-interior-design-black-paint-facade_73152-3913.jpg")`,
          backgroundSize: "150%",
          backgroundPosition: "center",
          objectFit: "cover",
        }}
      >
        <iframe
          className="absolute w-full h-full container px-32"
          title="Trailer"
          src={`https://www.youtube.com/embed/${trailer}`}
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        />
      </div>
      <div className="container mx-auto pt-10 space-y-4" id="lichChieuPhim">
        <span className="text-4xl font-bold">Lịch chiếu: {movie.tenPhim}</span>
        {movieSystem.length > 0 ? (
          <Tabs
            tabPosition="left"
            defaultActiveKey="1"
            items={renderMovieSystem()}
            onChange={onChange}
          />
        ) : (
          <div className="flex flex-col justify-center items-center">
            <Lottie
              style={{ height: 300 }}
              animationData={popcorn}
              loop={true}
            />
            <p className="text-red-600 font-bold text-3xl">
              Phim này hiện tại chưa có lịch chiếu
            </p>
          </div>
        )}
      </div>
      {modalData && (
        <Modal
          title={modalData.tenPhim}
          open={isModalOpen}
          onCancel={handleCancel}
          footer={[]}
        >
          <div className="flex space-x-2 items-center">
            <img width={150} src={modalData.hinhAnh} alt="Movie" />
            <div className="justify space-y-3">
              <div className="flex space-x-5">
                <p className="text-red-600 font-bold">
                  Ngày chiếu:{" "}
                  <span className="text-black">{modalData.ngayChieu}</span>
                </p>
                <p className="text-red-600 font-bold">
                  Giờ chiếu:{" "}
                  <span className="text-black">{modalData.gioChieu}</span>
                </p>
              </div>
              <p className="text-red-600 font-bold">
                Rạp đã chọn:{" "}
                <span className="text-black">{modalData.tenCumRap}</span>
              </p>
              <NavLink
                to={`/booking-movie/${id}`}
                className="w-full p-2 rounded bg-red-500 text-white transition hover:scale-105 hover:font-bold hover:bg-blue-400 hover:text-white flex justify-center items-center"
              >
                Thanh toán và đặt vé
              </NavLink>
            </div>
          </div>
        </Modal>
      )}
      <Footer />
    </>
  );
}
